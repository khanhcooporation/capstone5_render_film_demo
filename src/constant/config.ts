//Ghi PATH
export const PATH = {
    login:'/login',
    register:'/register',
    admin:'/admin',
    adminFilm:'/admin/film',
    adminFilmManage: '/admin/film/manage',
    adminFilmAdd: '/admin/film/addnew',
    //userAdmin
    adminUser:'/admin/user',
    account:'/account',
}

export default PATH