import { createSlice } from "@reduxjs/toolkit";
import {
  quanLyCumRapThunk,
  quanLyLichChieuTheoHeThongThunk,
  quanLyRapThunk,
} from "./thunk";
import { CumRapInfo, LichChieuTheoHeThong, RapSys } from "types/QuanLyRap";

type QuanLyRapInitialState = {
  heThongRap?: RapSys[];
  cumRap?: CumRapInfo[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  tenHeThongRapHienTai?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  tenCumRapHientai?: any;
  lichChieuTheoHeThong?: LichChieuTheoHeThong[];

  dateSort?:string;
  ktraFilm?:boolean;
};
const initialState: QuanLyRapInitialState = {
  heThongRap: [],
  cumRap: [],
  tenHeThongRapHienTai: "BHDStar",
};
export const quanLyRapSlice = createSlice({
  name: "quanLyRap",
  initialState,
  reducers: {
    LuuMaRap: (state, actions) => {
      state.tenHeThongRapHienTai = actions.payload;
    },
    LuuCumRap: (state, actions) => {
      state.tenCumRapHientai = actions.payload;
    },
    setDateSort:(state, actions) => {
      state.dateSort = actions.payload
    },
    setKtraFilm:(state, {payload}) => {
      state.ktraFilm =  payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(quanLyRapThunk.fulfilled, (state, { payload }) => {
        // console.log(payload)
        state.heThongRap = payload;
      })
      .addCase(quanLyCumRapThunk.fulfilled, (state, { payload }) => {
        state.cumRap = payload;
      })
      .addCase(
        quanLyLichChieuTheoHeThongThunk.fulfilled,
        (state, { payload }) => {
          state.lichChieuTheoHeThong = payload;
        }
      );
  },
});
export const { reducer: quanLyRapReducer, actions: quanLyRapActions } =
  quanLyRapSlice;
