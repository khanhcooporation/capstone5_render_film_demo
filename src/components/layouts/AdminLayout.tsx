import { AdminNavbar } from "components"

export const AdminLayout = () => {
  return (
    <div>
      <AdminNavbar/>
    </div>
  )
}

export default AdminLayout