import { EyeOutlined, ShoppingOutlined } from "@ant-design/icons";
import { DatePickerProps, Skeleton, Space } from "antd";
import { Card } from "components/ui";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { quanLyPhimThunk } from "store/quanLyPhim/thunk";
import cn from "classnames";
import style from "./index.module.scss";
import { useTheaterData } from "hooks/useTheaterData";

import {
  quanLyCumRapThunk,
  quanLyLichChieuTheoHeThongThunk,
} from "store/quanLyRap/thunk";
import { quanLyRapActions } from "store/quanLyRap/slice";
import DatePicker from "components/ui/DatePicker";

export const HomeTemplate = () => {
  const dispatch = useAppDispatch();
  const dispatchOrigin = useDispatch();

  // DatePicker
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log("date", date);
    // console.log("dateString", dateString);
    dispatchOrigin(quanLyRapActions.setDateSort(dateString)); //!==============
  };

  const { movieList, isFetchingMovieList } = useSelector(
    (state: RootState) => state.quanLyPhim
  );
  const {
    heThongRap,
    cumRap,
    tenHeThongRapHienTai,
    lichChieuTheoHeThong,
    tenCumRapHientai,
    dateSort, //!===============================
    ktraFilm,
  } = useTheaterData();
  // console.log("tenhethong", tenHeThongRapHienTai);
  console.log("dateSORT: ", dateSort);

  useEffect(() => {
    dispatch(quanLyPhimThunk());
  }, [dispatch]);

  useEffect(() => {
    dispatch(quanLyCumRapThunk(tenHeThongRapHienTai));
  }, [tenHeThongRapHienTai]);

  useEffect(() => {
    dispatch(quanLyLichChieuTheoHeThongThunk(tenHeThongRapHienTai));
  }, [tenHeThongRapHienTai]);

  console.log("lichChieu: ", lichChieuTheoHeThong?.[0]);
  // console.log("maRapHienTai: ", tenCumRapHientai);
  // console.log("cumRap: ", cumRap);
  // console.log('tencumraphientai: ', tenCumRapHientai)
  // console.log("lichChieuTheoHeThong?.[0].lstCumRap filter",data[0])

  if (isFetchingMovieList) {
    return (
      <div className="p-5 rounded-sm">
        <div className="grid grid-cols-4 gap-[30px] mt-10">
          {[...Array(16)].map((_, index) => {
            return (
              <Card key={index} className="!w-[240px]">
                <Skeleton.Image active className="!w-full !h-[250px]" />
                <Skeleton.Input active className="!w-full !mt-10" />
                <Skeleton.Input active className="!w-full !mt-10" />
              </Card>
            );
          })}
        </div>
      </div>
    );
  }
  return (
    <div className=" p-5 rounded-sm ">
      <div className="grid grid-cols-4 gap-[30px]">
        {movieList?.map((movie) => (
          // <div className="w-[240px] transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-110  duration-500">
          <Card
            key={movie.maPhim}
            // hoverable = {true}
            style={{ width: 240, border: "none" }}
            cover={
              <div className="relative">
                <img alt="example" src={movie.hinhAnh} className="" />
                <div className={cn(style.hoverOverlayLayer)}>
                  <button
                    className={cn(style.overlayButtonLeft, "text-yellow-500 ")}
                  >
                    <EyeOutlined />
                  </button>
                  <button
                    className={cn(style.overlayButtonRight, "text-yellow-500")}
                  >
                    <ShoppingOutlined />
                  </button>
                </div>
              </div>
            }
            className={cn(
              style.CardCSS,
              "transition ease-in-out delay-75 hover:-translate-y-1 hover:scale-105 duration-500 cursor-pointer"
            )}
          >
            <Card.Meta
              title={
                <p
                  className="text-blue-500 font-bold"
                  style={{ whiteSpace: "normal" }}
                >
                  {movie.tenPhim}
                </p>
              }
              description={
                <p className="text-red-500 flex justify-center">
                  {movie.moTa.substring(0, 50)}
                </p>
              }
              style={{ color: "red" }}
            />
          </Card>
        ))}
      </div>
      <div className="bg-[rgba(62,67,70,0.73)] mt-7 rounded-lg text-black flex ">
        {/* head cua he thong rap */}
        <div className="ml-2 w-[50px] !border-r-4 border-[#c85661]">
          {/* phải có border-r-4 trước rồi mới dùng border màu được */}
          {heThongRap?.map((map) => {
            return (
              <div
                key={map.maHeThongRap}
                className="w-[40px] py-2 hover:scale-110 transition ease-in-out duration-500"
              >
                <button
                  className="w-[40px]"
                  onClick={() => {
                    dispatchOrigin(quanLyRapActions.LuuMaRap(map.maHeThongRap));
                  }}
                >
                  <img className="w-[40px]" src={map.logo} alt="..." />
                </button>
              </div>
            );
          })}
        </div>
        {/* End cua he thong rap */}

        {/* head cua cum rap */}
        <div
          className={cn(
            style.customScrollbar,
            "w-50% ml-2 !border-r-4 border-[#c85661] pr-4 h-[60vh] overflow-y-auto "
          )}
        >
          {cumRap?.map((map) => {
            return (
              <div
                key={map?.maCumRap}
                className={cn(
                  "mb-3 cursor-pointer hover:bg-slate-300 transition ease-in-out duration-500 rounded",
                  { "bg-slate-300": map.maCumRap == tenCumRapHientai }
                )}
                onClick={() => {
                  dispatchOrigin(quanLyRapActions.LuuCumRap(map.maCumRap));
                }}
              >
                <div className="ml-2 flex py-2 items-center">
                  <img
                    className="w-[40px] h-[40px] "
                    src={
                      lichChieuTheoHeThong?.[0].lstCumRap.filter(
                        (prd) => prd?.maCumRap === map.maCumRap
                      )?.[0]?.hinhAnh || "/image/body/theater.jpg"
                    }
                    alt="..."
                  />
                  <div className="ml-2">
                    <p className="font-bold">{map?.tenCumRap}</p>
                    <p className="">{map?.diaChi.substring(0, 20)}...</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        {/* end cua cum rap */}
        {/* head cua lich chieu theo he thong */}
        <div
          className={cn(
            style.customScrollbar,
            "w-[600px] h-[60vh] overflow-y-auto relative"
          )}
        >
          <div
            className="w-full"
            style={{ textAlign: "right", position: "sticky", top: 0 }}
          >
            <div className=" sticky top-0">
              <Space direction="vertical">
                <DatePicker
                  style={{ background: "beige" }}
                  onChange={onChange}
                />
                {/* Note: chinh mau placeholder */}
              </Space>
            </div>
          </div>
          <div className="w-full">
            {lichChieuTheoHeThong?.[0].lstCumRap
              .filter((prd) => prd.maCumRap === tenCumRapHientai)
              .map((lstCumRap) => (
                <div>
                  {lstCumRap.danhSachPhim.map((dsPhim) => (
                    <div className="">
                      <div className="flex gap-2 ">
                        <img
                          src={dsPhim.hinhAnh}
                          alt="..."
                          className="w-1/5 h-1/5"
                        />
                        <div className="flex flex-col w-full">
                          <div className="flex gap-2">
                            <p className="font-bold ">{dsPhim.tenPhim}</p>
                            {dsPhim.hot && (
                              <p className="bg-red-500 text-xs px-1 text-white rounded-lg self-center">
                                HOT
                              </p>
                            )}
                          </div>
                          <div className=" gap-5">
                            {dsPhim.lstLichChieuTheoPhim.map(
                              (lstLichChieuTheoPhim) => {
                                if (dateSort) {
                                  return (
                                    <div className="">
                                       {(lstLichChieuTheoPhim.ngayChieuGioChieu.substring(0,10) === dateSort)? dispatch(quanLyRapActions.setKtraFilm(false)) : "2"}
                                      {lstLichChieuTheoPhim.ngayChieuGioChieu.substring(
                                        0,
                                        10
                                      ) === dateSort ? (
                                        <div className="flex justify-between border-b-2 border-red-800 mt-3">
                                          {/* <p className="">
                                            mã Phim:
                                            {lstLichChieuTheoPhim.maLichChieu}
                                          </p> */}
                                          <p className="text-green-600 font-semibold hover:text-white transition ease-in-out delay-75 duration-500 cursor:pointer">
                                            {lstLichChieuTheoPhim.ngayChieuGioChieu.substring(
                                              11,
                                              20
                                            )}
                                          </p>
                                          <p className="text-amber-500 font-semibold">
                                            {lstLichChieuTheoPhim.ngayChieuGioChieu.substring(
                                              0,
                                              10
                                            )}
                                          </p>
                                        </div>
                                      ) :  ""
                                      }
                                    </div>
                                  );
                                } else {
                                  return (
                                    <div className="">
                                      <div className="flex justify-between border-b-2 border-red-800 mt-3">
                                        {/* <p className="">
                                        mã Phim:
                                        {lstLichChieuTheoPhim.maLichChieu}
                                      </p> */}
                                        <p className="text-green-600 font-semibold hover:text-white transition ease-in-out delay-75 duration-500 cursor:pointer">
                                          {lstLichChieuTheoPhim.ngayChieuGioChieu.substring(
                                            11,
                                            20
                                          )}
                                        </p>
                                        <p className="text-amber-500 font-semibold">
                                          {lstLichChieuTheoPhim.ngayChieuGioChieu.substring(
                                            0,
                                            10
                                          )}
                                        </p>
                                      </div>
                                    </div>
                                  );
                                }
                              }
                            )}
                          </div>
                        </div>
                      </div>
                      <hr
                        className="mt-2 mb-2"
                        style={{ border: "2px solid #c85661", width: "100%" }}
                      />
                    </div>
                  ))}
                </div>
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeTemplate;
